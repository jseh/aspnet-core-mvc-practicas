﻿using CSharpAspMvcCore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CSharpAspMvcCore.Controllers
{
    public class PruebaController : Controller
    {
        // GET
        public IActionResult Index()
        {
            return View();
        }
        
        // Obtener informacion por medio del QueryString enviado por GET
        
        // lo tipos requeridos tienen que especificarse al principio siempre 
        [HttpGet]
        public string BindingTipoPrimitivo(int valor = 20)
        {
            return $"Valor: {valor}";
        }
        
        [HttpGet]
        public string BindingTipoPrimitivo2(string a, string b)
        {
            return $"Valor: {a}-{b}";
        }
        
        [HttpGet]
        public string BindingTipoPrimitivo3(int x, string a, string b)
        {
            return $"Valor: {x}-{a}-{b}";
        }
        
        [HttpGet]
        public string BindingTipoPrimitivo4(int x, string a, string b)
        {
            return $"Valor: {x}-{a}-{b}";
        }


        
        
        [HttpGet]
        public string BindingQueryString(Cliente cliente)
        {
            return cliente.Nombre;
        }
        
        
        [HttpGet]
        public string BindingRequestQueryString()
        {
            var yo = HttpContext.Request.Query["Nombre"].ToString();
            return yo;
        } 
     
        // Obtener informacion enviada por POST
        
        [HttpPost]
        public string BindingRequestData()
        {
            var id = HttpContext.Request.Form["Hey"];
            return "ok";
        } 
        
        
        [HttpPost]
        public string BindingModel(Cliente c)
        {
            return c.Nombre;
        } 
        
        [HttpPost]
        public string BindingModelFromBodyJson([FromBody]Cliente c)
        {
            return c.Nombre;
        } 
        
        [HttpPost]
        public string BindingFormCollection(IFormCollection f)
        {
            var nombre = f["nombre"];
            return nombre;
        }
        
        // [HttpPost]
        // public IActionResult Nuevo()
        // {
        //     return View();
        // }  
        
        
        
        [HttpGet]
        public string Nuevo()
        {
            return "";
        }
        
        [HttpPost]
        public string Crear(Cliente c)
        {
            return "creando modelo";
        }
        
         
        
        [HttpGet]
        public string Modificar(int id)
        {
            return "";
        }
        
        [HttpPost]
        public string Guardar(Cliente c)
        {
            return "guardando cambios";
        }
        
        
        
        
        [HttpGet]
        public void Mostrar(int id)
        {
          
        }
        
        [HttpGet]
        public void Eliminar(int id)
        {
          
        }
        
        
        
        [HttpGet]
        public IActionResult VueForm()
        {
            return View();
        }
        
    
    }
}